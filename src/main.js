import data from './Data.js';
import Component from './components/Component.js';
import Img from './components/Img.js';
console.log(data[0].image);
// const title = new Component( 'h1', null, 'La carte' );
// document.querySelector('.pageTitle').innerHTML = title.render();

const title = new Component( 'h1', null, ['La', ' ', 'carte'] );
document.querySelector('.pageTitle').innerHTML = title.render();


const img = new Img(data[0].image);
document.querySelector( '.pageContent' ).innerHTML = img.render();

