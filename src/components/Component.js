export default class Component {
	tagName;
	attribute;
	children;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}
	
	render(){
		if (this.children) {
			return this.renderChildren;
		}
		else {
			return this.renderAttribute();
		}
	}
	
	renderAttribute(){
		return `<${this.tagName} ${this.attribute.name} = "${this.attribute.value}" />`;
	}
	
	renderChildren(){
		let str = '';
		if(this.children instanceof Component) {
			this.children.forEach(element => {
				str += element
			});
			return `<${this.tagName}> ${this.str} </${this.tagName}>`;
		}
		else {
			return `<${this.tagName}> ${this.children} </${this.tagName}>`;
		}
	}
}
